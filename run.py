from modules import i2c_lcd as lcd
import threading
import time
import logging
import redis
from temperature import temp as temperature
from temperature import delete_older_rows as delete
import button


redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)


def get_logger():
    logger = logging.getLogger("threading_example")
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler("threading.log")
    fmt = '%(asctime)s - %(threadName)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    fh.setFormatter(formatter)

    logger.addHandler(fh)
    return logger


class LCD(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        self.daemon = True
        self.start()

    def run(self):
        while True:
            try:
                lcd.main()
            except KeyboardInterrupt:
                pass
            finally:
                lcd.lcd_byte(0x01, lcd.LCD_CMD)


class TemperatureDaemon(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True
        self.start()

    def run(self):
        while True:
            starttime = time.time()
            temperature()
            time.sleep(60.0 - ((time.time() - starttime) % 60.0))


class DeleteOlder(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

        self.daemon = True
        self.start()

    def run(self):
        while True:
            starttime = time.time()
            delete()
            time.sleep(600.0 - ((time.time() - starttime) % 600.0))


class LightsSwitches(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True
        self.start()

    def run(self):
        while True:
            button.test()


if __name__ == '__main__':
    LCD()
    TemperatureDaemon()
    DeleteOlder()
    LightsSwitches()
    while True:
        pass
