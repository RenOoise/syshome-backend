from modules import switches
import redis


redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)
sw = switches.GpioThings


def test():
    while True:
        status = sw.read_switch(1, 10)
        room_light = redis_db.get('room_light')

        if status == 1 and int(room_light) == 0:
            sw.switch_lights(1, 20, 1)
            print(status)
            redis_db.set('room_light', status)
        elif status == 0 and int(room_light) == 1:
            sw.switch_lights(1, 20, 0)
            redis_db.set('room_light', status)
            print(status)
        else:
            pass
