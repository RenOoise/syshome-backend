from modules import myconnutils, sensors
import redis
from datetime import datetime
from time import sleep
redis_db = redis.StrictRedis(host="localhost", port=6379, db=0)


# функция удаления старых записей
def delete_older_rows():
    connection = myconnutils.getConnection()
    try:
        cursor = connection.cursor()
        sql = "DELETE FROM `temperature_value` WHERE `timestamp` < ADDDATE(NOW(), INTERVAL -14 DAY)"
        cursor.execute(sql)
        connection.commit()
    finally:
        connection.close()

def temp():
    while True:
        connection = myconnutils.getConnection()
        try:
            cursor = connection.cursor()
            sql = "SELECT `id`, `w1_addr` FROM `sensor` WHERE `type`='w1temp' AND `function`='temp' AND `place`=place"
            cursor.execute(sql)
            row = cursor.fetchall()
            for i in range(len(row)):
                tor = row[i]
                id = tor['id']
                address = tor['w1_addr']
                temp = sensors.w1.w1_temp(1, address)
                timestamp = datetime.now()
                if temp is not None:
                    if float(temp) < float(50):
                        redis_db.set(address, temp)
                    ask_redis = redis_db.get(address)
                    if float(ask_redis) == float(temp):
                        sql = "INSERT INTO `temperature_value` (`sensor_id`, `value`, `timestamp`) VALUES (%s, %s, %s)"
                        cursor.execute(sql, (id, temp, timestamp))
                        print('Добавлено значение для сенсора с адресом ', address, temp, ' градусов')
                else:
                    print('Сенсор с адресом ', address, ' отвалился или не отвечает')
            connection.commit()
        finally:
            connection.close()
            sleep(60)



