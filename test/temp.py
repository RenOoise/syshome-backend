from modules import sensors
from modules import i2c_lcd as lcd
import time
from datetime import datetime


def main():
    # Main program block

    # Initialise display
    lcd.lcd_init()
    while True:
        one = sensors.w1.w1_temp(1, "28-0417524311ff")
        two = sensors.w1.w1_temp(1, "28-0417524311ff")
        three = sensors.w1.w1_temp(1, "28-0417524311ff")
        four = sensors.w1.w1_temp(1, "28-0417524311ff")
        five = sensors.w1.w1_temp(1, "28-0417524311ff")
        six = sensors.w1.w1_temp(1, "28-0417524311ff")
        # Send some test
        lcd.lcd_string("Temp:" + one + " C", lcd.LCD_LINE_1)
        lcd.lcd_string("Temp:" + two + " C", lcd.LCD_LINE_2)
        lcd.lcd_string("Temp:" + three + " C", lcd.LCD_LINE_3)
        lcd.lcd_string("Temp:" + four + " C", lcd.LCD_LINE_4)
        print('1st LCD screen generated')
        # Send some time
        time.sleep(5)

        lcd.lcd_string("Date: " + datetime.strftime(datetime.now(), "%d.%m.%y"), lcd.LCD_LINE_1)
        lcd.lcd_string("Time: " + datetime.strftime(datetime.now(), "%H:%M"), lcd.LCD_LINE_2)
        lcd.lcd_string("Temp:" + five + " C", lcd.LCD_LINE_3)
        lcd.lcd_string("Temp:" + six + " C", lcd.LCD_LINE_4)
        print('2nd LCD screen generated')


if __name__ == '__main__':

    try:
        main()
    except KeyboardInterrupt:
        pass
    finally:
        lcd.lcd_byte(0x01, lcd.LCD_CMD)
