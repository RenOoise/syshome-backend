import os
import time
from pyA20.gpio import gpio
from modules import dht11

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')


'''Определяем класс считывания всех сенсоров 1-wire'''


class w1(object):
    def w1_temp(self, address):
        base_dir = '/sys/bus/w1/devices/'
        device_folder = base_dir + address
        device_file = device_folder + '/w1_slave'

        def read_temp_raw():
            f = open(device_file, 'r')
            lines = f.readlines()
            f.close()
            return lines

        def read_temp():
            lines = read_temp_raw()
            # Analyze if the last 3 characters are 'YES'.
            while lines[0].strip()[-3:] != 'YES':
                time.sleep(0.2)
                lines = read_temp_raw()
            # Find the index of 't=' in a string.
            equals_pos = lines[1].find('t=')
            if equals_pos != -1:
                # Read the temperature .
                temp_string = lines[1][equals_pos + 2:]
                temp_c = float(temp_string) / 1000.0
                return temp_c

        try:
            temp_value = '%3.1f' % read_temp()
        except Exception as error:
            print(error)
            return None

        return temp_value


'''Определяем класс чтения сенсоров DHT'''


class dht(object):

    def dht11(self, pin):
        gpio.init()
        # Получаем данные с пина
        instance = dht11.DHT11(pin=pin)
        result = instance.read()
        if result.is_valid():
            humidity = result.humidity
            temperature = result.temperature
            result = {'temperature': temperature, 'humidity': humidity}
        return result


'''Класс считывания состояний кнопок, герконов релюшек'''


class InputSensors(object):
    def barier_output(self, pin):
        gpio.init()
        gpio.setcfg(pin, gpio.INPUT)
        gpio.pullup(pin, gpio.PULLUP)
        return gpio.input(pin)

    def trigger_output(self, pin):
        gpio.init()
        gpio.setcfg(pin, gpio.INPUT)
        gpio.pullup(pin, gpio.PULLUP)
        return gpio.input(pin)
