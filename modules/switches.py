from pyA20.gpio import gpio
from pyA20.gpio import port

gpio.init()


class GpioThings:

    def read_switch(self, pin):
        gpio.setcfg(pin, gpio.INPUT)
        gpio.pullup(pin, gpio.PULLUP)
        return gpio.input(pin)

    def switch_lights(self, pin, status):
        gpio.setcfg(port.PA20, gpio.OUTPUT)
        gpio.output(pin, status)
